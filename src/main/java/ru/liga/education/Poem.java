package ru.liga.education;

import java.util.List;

public class Poem {
    private final String author;
    private final List<String> lines;

    public Poem(String author, List<String> lines) {
        this.author = author;
        this.lines = lines;
    }

    public String getAuthor() {
        return author;
    }

    public List<String> getLines() {
        return lines;
    }
}
