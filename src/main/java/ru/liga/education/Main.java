package ru.liga.education;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        Poem poem1 = new Poem("Масаока Сики", ProcessingMethods.processingHaikuOne());
        Poem poem2 = new Poem("Танэда Сантока", ProcessingMethods.processingHaikuTwo());
        Poem poem3 = new Poem("Мацуо Басё", ProcessingMethods.processingHaikuThree());
        List<Poem> poems = List.of(poem1, poem2, poem3);
        //ProcessingMethods.processingPoem(poems);


        System.out.println(Utility.printProcessingPoem(poems, "Ь", "И", sentence -> sentence.stream().anyMatch(word -> word.length() == 1), word -> word.contains("СТ")));
        System.out.println(Utility.printProcessingPoem(poems, "ь", "и", sentence -> sentence.stream().anyMatch(word -> word.length() == 1), word -> word.contains("ст")));
/*
		System.out.println(Utility.printProcessingPoem(poems,"я","и"));
		System.out.println(Utility.printProcessingPoem(poems,"щ","м"));
*/

/*	Utility.printHaiku(ProcessingMethods::processingHaikuOne,Utility::printLowerCase);
	Utility.printHaiku(ProcessingMethods::processingHaikuTwo,Utility::printUpperCase);
	Utility.printHaiku(ProcessingMethods::processingHaikuThree,Utility::printDefaultCase);*/

    }

}
