package ru.liga.education;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utility {
    public static void printHaiku(Supplier<List<String>> poemLines, Consumer<String> lineHandler) {
        final List<String> lines = poemLines.get();
        for (String line : lines) {
            lineHandler.accept(line);
        }
    }

    public static void printLowerCase(String line) {
        System.out.println(line.toLowerCase());
    }

    public static void printUpperCase(String line) {
        System.out.println(line.toUpperCase());
    }
    public static void printDefaultCase(String line) {
        System.out.println(line);
    }



    public static Stream<String> poemsProcessingCheck(Collection<Poem> poems,Predicate<List<String>> suggestion, Predicate<String> word){
        return poems.stream()
                .map(Poem::getLines)
                .flatMap(Collection::stream)
                //   .collect(Collectors.toList());
                .map(line->List.of(line.split(" ")))
                .filter(suggestion)
                .flatMap(Collection::stream)
                .filter(word);
             //   .collect(Collectors.toList());
       // return returnList;
    }


    public static List<String> printProcessingPoem(Collection<Poem> poems, String symbContain, String symbStarts, Predicate<List<String>> sentence, Predicate<String> wordCount) {
        if (poemsProcessingCheck(poems, sentence, wordCount).anyMatch(word -> word.contains(symbContain))) {
            return poemsProcessingCheck(poems, sentence, wordCount).collect(Collectors.toList());
        }else {
            return poemsProcessingCheck(poems, sentence, wordCount).filter(word -> word.startsWith(symbStarts)).collect(Collectors.toList());
        }}


}
