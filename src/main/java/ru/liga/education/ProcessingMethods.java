package ru.liga.education;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProcessingMethods {

    public static List<String> processingHaikuOne() {
       String haiku= "Нарисован пион\nтушь и кисти так и остались\nлежать на блюде";
        List<String> items = Stream.of(haiku.split("\n"))
                .map(String::trim)
                .collect(Collectors.toList());
        return items;
    }

    public static List<String> processingHaikuTwo() {
        String stringOne= new String("Родные края");
        String stringTwo= new String("далеки далеки");
        String stringThree= new String("Почки на деревья");

        List<String> items = Stream.of(stringOne,stringTwo,stringThree)
                .map(String::trim)
                .map(String::toLowerCase)
                .collect(Collectors.toList());
        return items;

    }
    public static List<String> processingHaikuThree() {

        String stringOne = new String("Полевой цветок");
        String stringTwo = new String("В лучах заката меня");
        String stringThree = new String("Пленил на миг");
        List<String> immutableList = List.of(stringOne,stringTwo,stringThree);
        return immutableList;
    }





}
